#include <Function.hpp>						// For Function
#include <OptimisationFunctionsImpl.hpp>	// For this class.

#include <private/DefaultOptimisationFunctions.hpp>	// For default function set.

OptimisationFunctionsImpl::OptimisationFunctionsImpl(
		std::shared_ptr<Function> stateSpaceRepresentation,
		std::shared_ptr<Function> costFunction,
		std::shared_ptr<Function> lagrangeMultiplier,
		std::shared_ptr<Function> functionalGradient
	) :
		pStateSpaceRepresentation(stateSpaceRepresentation),
		pCostFunction(costFunction),
		pLagrangeMultiplier(lagrangeMultiplier),
		pFunctionalGradient(functionalGradient) {

}

OptimisationFunctionsImpl::OptimisationFunctionsImpl()
	:
		pStateSpaceRepresentation(std::make_shared<StateSpaceRepresentationDefault>()),
		pCostFunction(std::make_shared<CostFunctionDefault>()),
		pLagrangeMultiplier(std::make_shared<LagrangeMultiplierDefault>()),
		pFunctionalGradient(std::make_shared<FunctionalGradientDefault>()) {

}

double OptimisationFunctionsImpl::stateSpaceRepresentation(const std::vector<double> &arguments) const {
	return pStateSpaceRepresentation->calculateValue(arguments);
}

double OptimisationFunctionsImpl::costFunction(const std::vector<double> &arguments) const {
	return pCostFunction->calculateValue(arguments);
}

double OptimisationFunctionsImpl::lagrangeMultiplier(const std::vector<double> &arguments) const {
	return pLagrangeMultiplier->calculateValue(arguments);
}

double OptimisationFunctionsImpl::functionalGradient(const std::vector<double> &arguments) const {
	return pFunctionalGradient->calculateValue(arguments);
}
