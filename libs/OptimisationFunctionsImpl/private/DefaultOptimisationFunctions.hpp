#ifndef DEFAULT_OPTIMISATION_FUNCTIONS_HPP
#define DEFAULT_OPTIMISATION_FUNCTIONS_HPP

#include <Function.hpp>	// For Function class.
#include <numeric>		// For std::accumulate.

// Default set of Function classes used for optimisation.
// Hi = Li + pi+1 * fi

// Contains State Space Representation "-0.2*x + u"
// where x = args[0], u = args[1].
class StateSpaceRepresentationDefault : virtual public Function {
	double calculateValue(const std::vector<double> &args) const override {
		return -0.2*args.at(0) + args.at(1);
	}
};

// Contains Cost Function "x*x + u*u"
class CostFunctionDefault : virtual public Function {
	static constexpr auto addSquareSecondToFirst = [](double first, double second) {
											return std::move(first) + second*second;
										};

	double calculateValue(const std::vector<double> &args) const override {
		return std::accumulate(args.begin(), args.end(), 0.0, addSquareSecondToFirst);
	}
};

// Contains formula for Lagrange multipliers derived from default stateSpace (fi) and costFunction (Li).
// pi = dHi / dxi; d - partial derivative
// pi = 2*xi - 0.2*pi+1
// where x = args[0], u = args[1], p = args[2].
class LagrangeMultiplierDefault : virtual public Function {
	double calculateValue(const std::vector<double> &args) const override {
		return 2*args.at(0) - 0.2*args.at(2);
	}
};

// Contains formula for functional gradient reduced to input space, 
// derived from default stateSpace (fi) and costFunction (Li).
// bi = dHi / dui; d - partial derivative
// bi = 2*ui + pi+1
// where x = args[0], u = args[1], p = args[2].
class FunctionalGradientDefault : virtual public Function {
	double calculateValue(const std::vector<double> &args) const override {
		return 2*args.at(1) + args.at(2);
	}
};

#endif // DEFAULT_OPTIMISATION_FUNCTIONS_HPP
