#ifndef OPTIMISATION_FUNCTIONS_IMPL_HPP
#define OPTIMISATION_FUNCTIONS_IMPL_HPP

#include <vector>						// For std::vector.
#include <memory>						// For std::shared_ptr.
#include <OptimisationFunctions.hpp>	// For base class.

class Function;

class OptimisationFunctionsImpl : public OptimisationFunctions {
public:
	OptimisationFunctionsImpl(std::shared_ptr<Function> stateSpaceRepresentation,
		std::shared_ptr<Function> costFunction,
		std::shared_ptr<Function> lagrangeMultiplier,
		std::shared_ptr<Function> functionalGradient
	);
	OptimisationFunctionsImpl();

	double stateSpaceRepresentation(const std::vector<double> &arguments) const override;
	double costFunction(const std::vector<double> &arguments) const override;
	double lagrangeMultiplier(const std::vector<double> &arguments) const override;
	double functionalGradient(const std::vector<double> &arguments) const override;

private:
	std::shared_ptr<Function> pStateSpaceRepresentation;
	std::shared_ptr<Function> pCostFunction;
	std::shared_ptr<Function> pLagrangeMultiplier;
	std::shared_ptr<Function> pFunctionalGradient;
};

#endif //OPTIMISATION_FUNCTIONS_IMPL_HPP
