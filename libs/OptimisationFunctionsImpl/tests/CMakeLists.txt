cmake_minimum_required(VERSION 3.13)

enable_testing()
find_package(GTest REQUIRED)

add_executable(OptimisationFunctionsImplTest)
target_sources(OptimisationFunctionsImplTest
	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/OptimisationFunctionsImpl.test.cpp
)

target_link_libraries(OptimisationFunctionsImplTest
	PRIVATE
		OptimisationFunctionsImpl
		gtest
		gtest_main
)

add_test(NAME OptimisationFunctionsImplTest
	COMMAND OptimisationFunctionsImplTest
)
