#include <vector>
#include <memory>

#include <gtest/gtest.h>
#include <OptimisationFunctionsImpl.hpp>

using namespace std;

class TestCase {
public:
	TestCase(const vector<double>& arguments, double result) : arguments(arguments), result(result) {};
	
	vector<double> arguments;
	double result;
};

TEST(StateSpaceRepresentationDefault, BasicTests) {
	unique_ptr<OptimisationFunctions> optimFuncs = make_unique<OptimisationFunctionsImpl>(OptimisationFunctionsImpl());
	
	vector<TestCase> testCases = {
		TestCase(vector<double>({1, 2}), 1.8),
		TestCase(vector<double>({0, 2}), 2),
		TestCase(vector<double>({1.34, 0.1214}), -0.1466),
		TestCase(vector<double>({1.34, 0.1214, 124}), -0.1466),
		TestCase(vector<double>({1.34, 0.1214, -12, 98}), -0.1466)
	};
	
	for (auto const& testCase : testCases) {
		EXPECT_DOUBLE_EQ(optimFuncs->stateSpaceRepresentation(testCase.arguments), testCase.result);
	}
}

TEST(StateSpaceRepresentationDefault, ThrowOutOfRange) {
	unique_ptr<OptimisationFunctions> optimFuncs = make_unique<OptimisationFunctionsImpl>(OptimisationFunctionsImpl());
	
	vector<TestCase> testCases = {
		TestCase(vector<double>({1}), 0),
		TestCase(vector<double>({-125151}), 0),
		TestCase(vector<double>({}), 0)
	};
	
	for (auto const& testCase : testCases) {
		EXPECT_THROW(optimFuncs->stateSpaceRepresentation(testCase.arguments), std::out_of_range);
	}
}

TEST(CostFunctionDefault, BasicTests) {
	unique_ptr<OptimisationFunctions> optimFuncs = make_unique<OptimisationFunctionsImpl>(OptimisationFunctionsImpl());

	vector<TestCase> testCases = {
		TestCase(vector<double>({1,0}), 		1),
		TestCase(vector<double>({0,0}), 		0),
		TestCase(vector<double>({-1,0}), 		1),
		TestCase(vector<double>({-36,0}), 	1296),
		TestCase(vector<double>({3, 12}), 	153),
		TestCase(vector<double>({8, 20}), 	464),
		TestCase(vector<double>({0, 0}), 	0),
		TestCase(vector<double>({-1, -2}), 	5),
		TestCase(vector<double>({7, -6}), 	85),
		TestCase(vector<double>({-1.025, 62.544, -9.702}), 	4006.931365),
		TestCase(vector<double>({12, 4, -6, -1}), 	197)
	};
	
	for (auto const& testCase : testCases) {
		EXPECT_DOUBLE_EQ(optimFuncs->costFunction(testCase.arguments), testCase.result);
	}
}

TEST(LagrangeMultiplierDefault, BasicTests) {
	unique_ptr<OptimisationFunctions> optimFuncs = make_unique<OptimisationFunctionsImpl>(OptimisationFunctionsImpl());

	vector<TestCase> testCases = {
		TestCase(vector<double>({2, 0, -6}), 		 5.2),
		TestCase(vector<double>({2, 9, -6}), 		 5.2),
		TestCase(vector<double>({6.7, 3, -6}), 		14.6),
		TestCase(vector<double>({6.7, -15, -0.7}), 	13.54),
		TestCase(vector<double>({-6.7, 15, -0.3}), -13.34),
	};
	
	for (auto const& testCase : testCases) {
		EXPECT_DOUBLE_EQ(optimFuncs->lagrangeMultiplier(testCase.arguments), testCase.result);
	}
}

TEST(LagrangeMultiplierDefault, ThrowOutOfRange) {
	unique_ptr<OptimisationFunctions> optimFuncs = make_unique<OptimisationFunctionsImpl>(OptimisationFunctionsImpl());
	
	vector<TestCase> testCases = {
		TestCase(vector<double>({}), 		0),
		TestCase(vector<double>({1}), 		0),
		TestCase(vector<double>({1,0}), 	0),
		TestCase(vector<double>({-1,0}), 	0),
		TestCase(vector<double>({3,-5}), 	0),
	};
	
	for (auto const& testCase : testCases) {
		EXPECT_THROW(optimFuncs->lagrangeMultiplier(testCase.arguments), std::out_of_range);
	}
}

TEST(FunctionalGradientDefault, BasicTests) {
	unique_ptr<OptimisationFunctions> optimFuncs = make_unique<OptimisationFunctionsImpl>(OptimisationFunctionsImpl());

	vector<TestCase> testCases = {
		TestCase(vector<double>({2, 0, -6}), 	   -6),
		TestCase(vector<double>({2, 9, -6}), 		12),
		TestCase(vector<double>({-23, 9, -6}), 		12),
		TestCase(vector<double>({6.7, 3, 6}), 		12),
		TestCase(vector<double>({-6.7, 15, -0.3}), 	29.7),
	};
	
	for (auto const& testCase : testCases) {
		EXPECT_DOUBLE_EQ(optimFuncs->functionalGradient(testCase.arguments), testCase.result);
	}
}

TEST(FunctionalGradientDefault, ThrowOutOfRange) {
	unique_ptr<OptimisationFunctions> optimFuncs = make_unique<OptimisationFunctionsImpl>(OptimisationFunctionsImpl());
	
	vector<TestCase> testCases = {
		TestCase(vector<double>({}), 		0),
		TestCase(vector<double>({1}), 		0),
		TestCase(vector<double>({1,0}), 	0),
		TestCase(vector<double>({-1,0}), 	0),
		TestCase(vector<double>({3,-5}), 	0),
	};
	
	for (auto const& testCase : testCases) {
		EXPECT_THROW(optimFuncs->functionalGradient(testCase.arguments), std::out_of_range);
	}
}