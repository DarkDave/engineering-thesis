#include "SolverWithBoundaries.hpp"

#include <vector>	// For std::vector.

SolverWithBoundaries::SolverWithBoundaries(std::shared_ptr<OptimisationFunctions> optimFuncs)
	:	optimFuncs(optimFuncs),
		bVec(),
		boundaries(std::make_shared<OptimisationBoundaries>(OptimisationBoundaries())),
		iterationsOfCoefficientOptimisation(0) {
}

OptimisationData SolverWithBoundaries::solve(std::shared_ptr<OptimisationData> initialData, const OptimisationBrake &brake) {
	OptimisationData outcome(*initialData);

	size_t i = 0;
	for (; i < brake.maxIterations; ++i) {
		outcome = advanceOneStep(std::make_shared<OptimisationData>(outcome));

		double gradientNorm = 0;
		for (auto const & b : bVec) {
			gradientNorm += b*b;
		}
		if (gradientNorm <= brake.accuracy) {
			break;
		}
	}
	return outcome;
}

OptimisationData SolverWithBoundaries::advanceOneStep(std::shared_ptr<OptimisationData> currentData) {
	OptimisationData newData(*currentData);
	
	calculateStateSpaceRepresentation(newData);

	const auto pVec = calculateLagrangeMultipliers(newData);

	calculateFunctionalGradients(newData, pVec);

	newData = calculateNewInputs(newData, calculateGradientImprovementCoefficient(newData));

	return newData;
}

void SolverWithBoundaries::setOptimisationFunctions(std::shared_ptr<OptimisationFunctions> optimFuncs) {
	this->optimFuncs = optimFuncs;
}

OptimisationBoundariesAvailable SolverWithBoundaries::setOptimisationBoundaries(std::shared_ptr<OptimisationBoundaries> boundaries) {
	this->boundaries = boundaries;
	return OptimisationBoundariesAvailable::Available;
}

void SolverWithBoundaries::calculateStateSpaceRepresentation(OptimisationData &data) {
	const auto N = data.u.size();
	data.x.resize(N+1);
	for (size_t i = 0; i < N; ++i) {
		data.x[i+1] = optimFuncs->stateSpaceRepresentation({data.x[i], data.u[i]});
	}
}

void SolverWithBoundaries::setIterationsOfCoefficientOptimisation(const uint8_t iterationsOfCoefficientOptimisation) {
	this->iterationsOfCoefficientOptimisation = iterationsOfCoefficientOptimisation;
}

OptimisationData SolverWithBoundaries::calculateNewInputs(const OptimisationData &data, const double gradientImprovementCoefficient) {
	const auto N = data.u.size();
	auto newData(data);
	for (size_t i = 0; i < N; ++i) {
		newData.u[i] = data.u[i] - gradientImprovementCoefficient * bVec[i];
	}
	calculateStateSpaceRepresentation(newData);
	return newData;
}

std::vector<double> SolverWithBoundaries::calculateLagrangeMultipliers(const OptimisationData &data) {
	const auto N = data.u.size();
	std::vector<double> pVec(N+1);
	pVec[N] = 0;
	for (int64_t i = static_cast<int64_t>(N-1); i > -1; --i) {
		pVec[i] = optimFuncs->lagrangeMultiplier({data.x[i], data.u[i], pVec[i+1]});

		pVec[i] += calculateLowerBoundaryPenalty(boundaries->getXMin(), data.x[i])
				+ calculateUpperBoundaryPenalty(boundaries->getXMax(), data.x[i]);
	}
	return pVec;
}

void SolverWithBoundaries::calculateFunctionalGradients(const OptimisationData &data, const std::vector<double> &pVec) {
	const auto N = data.u.size();
	bVec.resize(N);
	for (size_t i = 0; i < N; ++i) {
		bVec[i] = optimFuncs->functionalGradient({data.x[i], data.u[i], pVec[i+1]});

		bVec[i] += calculateLowerBoundaryPenalty(boundaries->getUMin(), data.u[i])
				+ calculateUpperBoundaryPenalty(boundaries->getUMax(), data.u[i]);
	}
}

double SolverWithBoundaries::calculateLowerBoundaryPenalty(const std::unique_ptr<double> &boundary, const double value) {
	if (boundary && value < *boundary) {
		return 2 * boundaries->boundariesCoefficient * (value - *boundary);
	}
	else {
		return 0.0;
	}
}

double SolverWithBoundaries::calculateUpperBoundaryPenalty(const std::unique_ptr<double> &boundary, const double value) {
	if (boundary && value > *boundary) {
		return 2 * boundaries->boundariesCoefficient * (value - *boundary);
	}
	else {
		return 0.0;
	}
}

double SolverWithBoundaries::calculateGradientImprovementCoefficient(const OptimisationData &data) {
	if (iterationsOfCoefficientOptimisation == 0) {
		return 0.01;
	}
	
	double lBound = 0;
	double lCost = calcualateTotalCost(calculateNewInputs(data, lBound));
	double rBound = 1000;
	double rCost = calcualateTotalCost(calculateNewInputs(data, rBound));
	
	for (uint8_t i = 0; i < iterationsOfCoefficientOptimisation; ++i) {
		const double middleBound = (rBound + lBound) / 2.0;
		const double middleCost = calcualateTotalCost(calculateNewInputs(data, middleBound));
		if (lCost < rCost) {
			rBound = middleBound;
			rCost = middleCost;
		}
		else {
			lBound = middleBound;
			lCost = middleCost;
		}
	}

	return (lBound+rBound) / 2.0;
}

double SolverWithBoundaries::calcualateTotalCost(const OptimisationData &data) {
	const auto N = data.u.size();
	double sum = 0;
	for (size_t i = 0; i < N; ++i) {
		sum += optimFuncs->costFunction({data.x[i], data.u[i]});

		if (boundaries->getXMin() && data.x[i] < *boundaries->getXMin()) {
			sum+= boundaries->boundariesCoefficient * (data.x[i] - *boundaries->getXMin()) * (data.x[i] - *boundaries->getXMin());
		}
		if (boundaries->getUMin() && data.u[i] < *boundaries->getUMin()) {
			sum+= boundaries->boundariesCoefficient * (data.u[i] - *boundaries->getUMin()) * (data.u[i] - *boundaries->getUMin());
		}

		if (boundaries->getXMax() && data.x[i] > *boundaries->getXMax()) {
			sum += boundaries->boundariesCoefficient * (data.x[i] - *boundaries->getXMax()) * (data.x[i] - *boundaries->getXMax());
		}
		if (boundaries->getUMax() && data.u[i] > *boundaries->getUMax()) {
			sum += boundaries->boundariesCoefficient * (data.u[i] - *boundaries->getUMax()) * (data.u[i] - *boundaries->getUMax());
		}
	}
	return sum;
}
