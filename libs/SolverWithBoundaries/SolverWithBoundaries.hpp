#ifndef SOLVER_WITH_BOUNDARIES_HPP
#define SOLVER_WITH_BOUNDARIES_HPP

#include <memory> // For std::shared_ptr.
#include <vector> // For std::vector.

#include <OptimisationBoundaries.hpp>	// For OptimisationBoundaries.
#include <OptimisationData.hpp>			// For OptimisationData.
#include <OptimisationFunctions.hpp>	// For OptimisationFunctions.
#include <Solver.hpp>					// For base class.

class SolverWithBoundaries : virtual public Solver {
public:
	SolverWithBoundaries(std::shared_ptr<OptimisationFunctions> optimFuncs);
	OptimisationData solve(std::shared_ptr<OptimisationData> initialData, const OptimisationBrake &brake) override;
	OptimisationData advanceOneStep(std::shared_ptr<OptimisationData> currentData) override;
	void setOptimisationFunctions(std::shared_ptr<OptimisationFunctions> optimFuncs) override;
	
	virtual OptimisationBoundariesAvailable setOptimisationBoundaries(std::shared_ptr<OptimisationBoundaries> boundaries) override;

	virtual void setIterationsOfCoefficientOptimisation(const uint8_t iterationsOfCoefficientOptimisation) override;
private:
	void calculateStateSpaceRepresentation(OptimisationData &data);
	OptimisationData calculateNewInputs(const OptimisationData &data, const double gradientImprovementCoefficient);
	std::vector<double> calculateLagrangeMultipliers(const OptimisationData &data);
	void calculateFunctionalGradients(const OptimisationData &data, const std::vector<double> &pVec);

	double calculateLowerBoundaryPenalty(const std::unique_ptr<double> &boundary, const double value);
	double calculateUpperBoundaryPenalty(const std::unique_ptr<double> &boundary, const double value);
	double calculateGradientImprovementCoefficient(const OptimisationData &data);

	double calcualateTotalCost(const OptimisationData &data);

	std::shared_ptr<OptimisationFunctions> optimFuncs;
	std::vector<double> bVec;
	std::shared_ptr<OptimisationBoundaries> boundaries;
	uint8_t iterationsOfCoefficientOptimisation;
};

#endif // SOLVER_WITH_BOUNDARIES_HPP
