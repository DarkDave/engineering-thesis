#ifndef SOLVER_HPP
#define SOLVER_HPP

#include <memory> // For std::shared_ptr.

#include <OptimisationBrake.hpp>		// For OptimisationBrake.
#include <OptimisationBoundaries.hpp>	// For OptimisationBoundaries.
#include <OptimisationData.hpp>			// For OptimisationData.
#include <OptimisationFunctions.hpp>	// For OptimisationFunctions.

enum class OptimisationBoundariesAvailable {
	Unavailable,
	Available
};

class Solver {
public:
	virtual OptimisationData solve(std::shared_ptr<OptimisationData> initialData, const OptimisationBrake &brake) = 0;
	virtual OptimisationData advanceOneStep(std::shared_ptr<OptimisationData> currentData) = 0;
	virtual void setOptimisationFunctions(std::shared_ptr<OptimisationFunctions> optimFuncs) = 0;

	virtual OptimisationBoundariesAvailable setOptimisationBoundaries(std::shared_ptr<OptimisationBoundaries>) {
		return OptimisationBoundariesAvailable::Unavailable;
	}

	virtual void setIterationsOfCoefficientOptimisation(const uint8_t iterationsOfCoefficientOptimisation) {
		(void) iterationsOfCoefficientOptimisation; // To leave variale name visible in function declaration.
	}

	virtual ~Solver() {};
};

#endif // SOLVER_HPP
