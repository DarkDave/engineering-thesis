#ifndef OPTIMISATION_BRAKE_HPP
#define OPTIMISATION_BRAKE_HPP

#include <cstddef> // For size_t.
#include <cstdint> // For uint8_t.

class OptimisationBrake {
public:
	OptimisationBrake(double accuracy, size_t maxIterations)
		:	accuracy(accuracy),
			maxIterations(maxIterations) {};
	double accuracy;
	size_t maxIterations;
};

#endif // OPTIMISATION_BRAKE_HPP
