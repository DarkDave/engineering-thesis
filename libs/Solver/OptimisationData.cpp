#include <OptimisationData.hpp>

#include <algorithm> // For for_each.

std::ostream& operator<<(std::ostream& os, const OptimisationData& data) {
	os << "\nu:";
	for_each (data.u.begin(), data.u.end(), [&os](const auto &d) {os << ' ' << d;} );
	os << "\nx:";
	for_each (data.x.begin(), data.x.end(), [&os](const auto &d) {os << ' ' << d;} );
	os << '\n';
	return os;
}
