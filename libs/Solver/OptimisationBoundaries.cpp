#include "OptimisationBoundaries.hpp"

OptimisationBoundaries::OptimisationBoundaries(double boundariesCoefficient)
	:	boundariesCoefficient(boundariesCoefficient),
		xMin(nullptr),
		xMax(nullptr),
		uMin(nullptr),
		uMax(nullptr) {
}

OptimisationBoundaries::OptimisationBoundaries(std::unique_ptr<double> &&xMin,
		std::unique_ptr<double> &&xMax,
		std::unique_ptr<double> &&uMin,
		std::unique_ptr<double> &&uMax,
		double boundariesCoefficient
	) :	boundariesCoefficient(boundariesCoefficient),
		xMin(std::move(xMin)),
		xMax(std::move(xMax)),
		uMin(std::move(uMin)),
		uMax(std::move(uMax)) {
}

void OptimisationBoundaries::setXMin(std::unique_ptr<double> xMin) {
    this->xMin = std::move(xMin);
}

std::unique_ptr<double> const& OptimisationBoundaries::getXMin() {
    return xMin;
}

void OptimisationBoundaries::setXMax(std::unique_ptr<double> xMax) {
	this->xMax = std::move(xMax);
}

std::unique_ptr<double> const& OptimisationBoundaries::getXMax() {
    return xMax;
}

void OptimisationBoundaries::setUMin(std::unique_ptr<double> uMin) {
	this->uMin = std::move(uMin);
}

std::unique_ptr<double> const& OptimisationBoundaries::getUMin() {
    return uMin;
}

void OptimisationBoundaries::setUMax(std::unique_ptr<double> uMax) {
	this->uMax = std::move(uMax);
}

std::unique_ptr<double> const& OptimisationBoundaries::getUMax() {
    return uMax;
}
