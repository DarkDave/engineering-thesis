#ifndef OPTIMISATION_BOUNDARIES_HPP
#define OPTIMISATION_BOUNDARIES_HPP

#include <memory> // For std::unique_ptr.

class OptimisationBoundaries {
	public:
		OptimisationBoundaries(double boundariesCoefficient = 100);
		OptimisationBoundaries(std::unique_ptr<double> &&xMin,
							std::unique_ptr<double> &&xMax,
							std::unique_ptr<double> &&uMin,
							std::unique_ptr<double> &&uMax,
							double boundariesCoefficient = 100
						);

		void setXMin(std::unique_ptr<double> xMin);
		std::unique_ptr<double> const& getXMin();

		void setXMax(std::unique_ptr<double> xMax);
		std::unique_ptr<double> const& getXMax();

		void setUMin(std::unique_ptr<double> uMin);
		std::unique_ptr<double> const& getUMin();

		void setUMax(std::unique_ptr<double> uMax);
		std::unique_ptr<double> const& getUMax();

		double boundariesCoefficient;
	private:
		std::unique_ptr<double> xMin;
		std::unique_ptr<double> xMax;
		std::unique_ptr<double> uMin;
		std::unique_ptr<double> uMax;
};

#endif // OPTIMISATION_BOUNDARIES_HPP
