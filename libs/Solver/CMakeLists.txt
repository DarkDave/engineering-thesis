cmake_minimum_required(VERSION 3.13)

project(Solver)

add_library(Solver)

target_include_directories(Solver
	PUBLIC
		${CMAKE_CURRENT_LIST_DIR}
)

target_sources(Solver
	PRIVATE
		OptimisationBrake.hpp
		OptimisationBoundaries.cpp
		OptimisationBoundaries.hpp
		OptimisationData.cpp
		OptimisationData.hpp
		Solver.hpp
)

target_link_libraries(Solver
	INTERFACE
		OptimisationFunctions
)
