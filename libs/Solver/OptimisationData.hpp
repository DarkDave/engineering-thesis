#ifndef OPTIMISATION_DATA_HPP
#define OPTIMISATION_DATA_HPP

#include <utility> 	// For std::move.
#include <vector> 	// For std::vector.
#include <ostream>  // For std::ostream.


class OptimisationData {
public:
	OptimisationData() : u(std::vector<double>()), x(std::vector<double>()) {};
	OptimisationData(const std::vector<double> &u, const std::vector<double> &x) : u(u), x(x) {};
	OptimisationData(const OptimisationData &data) : u(data.u), x(data.x) {};
	OptimisationData(OptimisationData &&data) : u(std::move(data.u)), x(std::move(data.x)) {};

	OptimisationData& operator=(const OptimisationData& d) {
		this->u = d.u;
		this->x = d.x;
		return *this;
	}
	
	std::vector<double> u;
	std::vector<double> x;
};

std::ostream& operator<<(std::ostream& os, const OptimisationData& data);

#endif // OPTIMISATION_DATA_HPP
