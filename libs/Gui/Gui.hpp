#ifndef GUI_HPP
#define GUI_HPP

#include <memory>
#include <vector>

#include <QMainWindow>
#include <QStringList>

namespace QtCharts {
class QChart;
class QValueAxis;
}
class QHBoxLayout;
class QTableWidget;
class QVBoxLayout;
class QLabel;

#include <OptimisationBoundaries.hpp>
#include <OptimisationData.hpp>
#include <Solver.hpp>

class Gui : public QMainWindow {
	Q_OBJECT

public:
	Gui(std::shared_ptr<Solver> solver, QWidget *parent = nullptr);

private slots:
	void advanceOneStepReleased();
	void solveReleased();
	void setInitialData();
	void setOptimisationBrake();
	void setOptimisationBoundaries();

private:
	void initialize();
	void createOptimisationBoundariesLayout();
	QVBoxLayout *createOptimisationBrakeLayout();

	void clearLayout(QLayout *pLayout);
	void resetToInitialData();

	void updateDisplay();
	void updateTable();

	uint8_t numberOfStepsToShow();

	// No copy and assignment.
	Gui(const Gui&) = delete;
	Gui& operator=(const Gui&) = delete;

	std::shared_ptr<Solver> pSolver;
	std::shared_ptr<OptimisationData> pCurrentData;
	std::shared_ptr<OptimisationData> pInitialData;
	std::shared_ptr<OptimisationBrake> pBrake;
	std::shared_ptr<OptimisationBoundaries> pBoundaries;

	QHBoxLayout *pOptimisationBoundariesLayout;

	QLabel *pAccuracyLabel;
	QLabel *pIterationsLabel;
	QLabel *pCoefficientLabel;
	QLabel *pCoefIterLabel;

	std::vector<OptimisationData> dataHistory;

	QtCharts::QChart *pStateChart;
	QtCharts::QChart *pInputChart;

	QtCharts::QValueAxis *pStateAxis;
	QtCharts::QValueAxis *pInputAxis;

	QTableWidget *pDataTable;
	QStringList tableHeader;

	uint8_t maxNumberOfDisplayedSteps;
	size_t optimisationHorizon;
	uint8_t iterationsOfCoefficientOptimisation;
};

#endif // GUI_HPP
