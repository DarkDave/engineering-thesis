#ifndef BOUNDARIES_DIALOG_HPP
#define BOUNDARIES_DIALOG_HPP

#include <memory>	// For std::shared_ptr.
#include <QDialog>	// For QDialog.

class OptimisationBoundaries;
class QPushButton;
class QDoubleSpinBox;

class BoundariesDialog : public QDialog {
	Q_OBJECT

public:
	BoundariesDialog(QWidget *parent = nullptr);

	std::shared_ptr<OptimisationBoundaries> getBoundaries();

private slots:
	void saveReleased();

private:
	// No copy and assignment.
	BoundariesDialog(const BoundariesDialog&) = delete;
	BoundariesDialog& operator=(const BoundariesDialog&) = delete;

	QPushButton *pSave;
	QPushButton *pCancel;

	QDoubleSpinBox *spinBoxes[4];
	QDoubleSpinBox *pCoefficientSpinBox;
};

#endif // BOUNDARIES_DIALOG_HPP
