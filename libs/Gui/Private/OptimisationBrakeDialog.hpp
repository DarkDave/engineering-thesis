#ifndef OPTIMISATION_BRAKE_DIALOG_HPP
#define OPTIMISATION_BRAKE_DIALOG_HPP

#include <QDialog>	// For QDialog.

#include <OptimisationBrake.hpp>

class QHBoxLayout;
class QPushButton;
class QSpinBox;

class OptimisationBrakeDialog : public QDialog {
	Q_OBJECT

public:
	OptimisationBrakeDialog(QWidget *parent = nullptr);

	OptimisationBrake getOptimisationBrake();
	uint8_t getIterationsOfCoefficientOptimisation();

private:
	// No copy and assignment.
	OptimisationBrakeDialog(const OptimisationBrakeDialog&) = delete;
	OptimisationBrakeDialog& operator=(const OptimisationBrakeDialog&) = delete;

	QHBoxLayout *createButtonLayout();
	QHBoxLayout *createDataLayout();

	QSpinBox *pAccuracy;
	QSpinBox *pIterations;
	QSpinBox *pCoefIter;

	QPushButton *pSave;
	QPushButton *pCancel;
};

#endif // OPTIMISATION_BRAKE_DIALOG_HPP
