#include "OptimisationBrakeDialog.hpp"

#include <cmath>

#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QSpinBox>
#include <QVBoxLayout>

OptimisationBrakeDialog::OptimisationBrakeDialog(QWidget *parent)
		:	QDialog(parent),
			pAccuracy(new QSpinBox()),
			pIterations(new QSpinBox()),
			pCoefIter(new QSpinBox()),
			pSave(new QPushButton("Save")),
			pCancel(new QPushButton("Cancel")) {

	QVBoxLayout *mainLayout = new QVBoxLayout(this);
	mainLayout->addLayout(createDataLayout());
	mainLayout->addLayout(createButtonLayout());
}

OptimisationBrake OptimisationBrakeDialog::getOptimisationBrake() {
	return OptimisationBrake(pow(10, (-pAccuracy->value())), pIterations->value());
}

uint8_t OptimisationBrakeDialog::getIterationsOfCoefficientOptimisation() {
	return static_cast<uint8_t>(pCoefIter->value());
}

QHBoxLayout *OptimisationBrakeDialog::createDataLayout() {
	QLabel *pAccLabel = new QLabel("<h2>Functional gradient accuracy: 10<sup> -</sup></h2>");
	QLabel *pIterLabel = new QLabel("<h2>Maximum number of iterations:</h2>");
	QLabel *pCoefIterLabel = new QLabel("<h2>Number of iterations for calculating gradient improvement coefficient:</h2>");
	QVBoxLayout *pLabelLayout = new QVBoxLayout();
	pLabelLayout->addWidget(pAccLabel);
	pLabelLayout->addWidget(pIterLabel);
	pLabelLayout->addWidget(pCoefIterLabel);

	pAccuracy->setRange(1, 100);
	pAccuracy->setSingleStep(1);
	pAccuracy->setValue(10);

	pIterations->setRange(1000, 1'000'000);
	pIterations->setSingleStep(10'000);
	pIterations->setValue(10'000);

	pCoefIter->setRange(0, 255);
	pCoefIter->setSingleStep(1);
	pCoefIter->setValue(50);

	QVBoxLayout *pSpinLayout = new QVBoxLayout();
	pSpinLayout->addWidget(pAccuracy);
	pSpinLayout->addWidget(pIterations);
	pSpinLayout->addWidget(pCoefIter);

	QHBoxLayout *pLayout = new QHBoxLayout();
	pLayout->addLayout(pLabelLayout);
	pLayout->addLayout(pSpinLayout);
	return pLayout;
}

QHBoxLayout *OptimisationBrakeDialog::createButtonLayout() {
	connect(pSave, &QPushButton::released, this, &OptimisationBrakeDialog::accept);
	connect(pCancel, &QPushButton::released, this, &OptimisationBrakeDialog::reject);

	QHBoxLayout *pButtonLayout = new QHBoxLayout();
	pButtonLayout->addWidget(pSave);
	pButtonLayout->addWidget(pCancel);

	return pButtonLayout;
}
