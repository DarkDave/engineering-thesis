#ifndef INITIAL_DATA_DIALOG_HPP
#define INITIAL_DATA_DIALOG_HPP

#include <QDialog>	// For QDialog.

#include <OptimisationData.hpp>

class QHBoxLayout;
class QPushButton;
class QTableWidget;

class InitialDataDialog : public QDialog {
	Q_OBJECT

public:
	InitialDataDialog(QWidget *parent = nullptr);

	OptimisationData getInitialData();

private slots:
	void saveReleased();

private:
	// No copy and assignment.
	InitialDataDialog(const InitialDataDialog&) = delete;
	InitialDataDialog& operator=(const InitialDataDialog&) = delete;

	QHBoxLayout *createOptimisationHorisonLayout();
	QTableWidget *createDataTable();
	QHBoxLayout *createButtonLayout();

	void setTableDataTypeToDouble();

	QTableWidget *pDataTable;

	QPushButton *pSave;
	QPushButton *pCancel;
};

#endif // INITIAL_DATA_DIALOG_HPP
