#include "InitialDataDialog.hpp"

#include <QPushButton>
#include <QTableWidget>
#include <QDialogButtonBox>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSpinBox>
#include <QHeaderView>
#include <QLabel>
#include <QMessageBox>

#include <QDebug>

static const int defaultOptimisationHorison = 4;

InitialDataDialog::InitialDataDialog(QWidget *parent)
		:	QDialog(parent),
			pDataTable(createDataTable()),
			pSave(new QPushButton("Save")),
			pCancel(new QPushButton("Cancel")) {

	QVBoxLayout *mainLayout = new QVBoxLayout(this);
	mainLayout->addLayout(createOptimisationHorisonLayout());
	mainLayout->addWidget(pDataTable);
	mainLayout->addLayout(createButtonLayout());

	setTableDataTypeToDouble();
}

OptimisationData InitialDataDialog::getInitialData() {
	OptimisationData initialData;
	initialData.x.push_back(pDataTable->item(0,1)->text().toDouble());
	for (int i = 0; i < pDataTable->rowCount(); ++i) {
		initialData.u.push_back(pDataTable->item(i,2)->text().toDouble());
	}

	return initialData;
}

void InitialDataDialog::saveReleased() {
	if(!pDataTable->item(0,0)) {
		QMessageBox::warning(this, "", "Enter initial state in 0th iteration.");
		return;
	}
	for (int i = 0; i < pDataTable->rowCount(); ++i) {
		if(!pDataTable->item(i,1)) {
			QMessageBox::warning(this, "", "Enter all input values in optimisation horison range.");
			return;
		}
	}

	this->accept();
}

QHBoxLayout *InitialDataDialog::createOptimisationHorisonLayout() {
	QLabel *pOptimisationHorisonDesc = new QLabel("Optimisation horizon");
	QSpinBox *pOptimisationHorison = new QSpinBox();
	pOptimisationHorison->setRange(1, 1000);
	pOptimisationHorison->setSingleStep(1);
	pOptimisationHorison->setValue(defaultOptimisationHorison);
	connect(pOptimisationHorison, QOverload<int>::of(&QSpinBox::valueChanged), [=](int i){
		pDataTable->setRowCount(i);
		setTableDataTypeToDouble();
	});

	QHBoxLayout *optimLayout = new QHBoxLayout();
	optimLayout->addWidget(pOptimisationHorisonDesc);
	optimLayout->addWidget(pOptimisationHorison);

	return optimLayout;
}

QTableWidget *InitialDataDialog::createDataTable() {
	QTableWidget *pDataTable = new QTableWidget();

	pDataTable->setRowCount(defaultOptimisationHorison);
	pDataTable->setColumnCount(3);
	pDataTable->setHorizontalHeaderLabels(QStringList({"n", "State", "Input"}));
	pDataTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	pDataTable->verticalHeader()->setVisible(false);

	return pDataTable;
}

QHBoxLayout *InitialDataDialog::createButtonLayout() {
	connect(pSave, &QPushButton::released, this, &InitialDataDialog::saveReleased);
	connect(pCancel, &QPushButton::released, this, &InitialDataDialog::reject);

	QHBoxLayout *pButtonLayout = new QHBoxLayout();
	pButtonLayout->addWidget(pSave);
	pButtonLayout->addWidget(pCancel);

	return pButtonLayout;
}

void InitialDataDialog::setTableDataTypeToDouble() {
	static const int doubleType = 2;
	for (int r = 0; r < pDataTable->rowCount(); ++r) {
		QTableWidgetItem *number = new QTableWidgetItem(QString("%1").arg(r));
		number->setTextAlignment(Qt::AlignCenter);
		number->setFlags(number->flags() & ~Qt::ItemIsEditable);
		pDataTable->setItem(r, 0, number);

		for (int c = 1; c < pDataTable->columnCount(); ++c) {
			QTableWidgetItem *item = new QTableWidgetItem(doubleType);
			item->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
			item->setData(Qt::DisplayRole, 0.0);
			pDataTable->setItem(r, c, item);
		}
	}
}
