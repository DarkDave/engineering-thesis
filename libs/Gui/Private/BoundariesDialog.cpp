#include "BoundariesDialog.hpp"

#include <limits>

#include <OptimisationBoundaries.hpp>

#include <QCheckBox>
#include <QDoubleSpinBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>

BoundariesDialog::BoundariesDialog(QWidget *parent)
		:	QDialog(parent),
			pSave(new QPushButton("Save")),
			pCancel(new QPushButton("Cancel")),
			pCoefficientSpinBox(new QDoubleSpinBox()) {
	const QStringList names = {"xMin", "xMax", "uMin", "uMax"};

	QVBoxLayout *pCheckBoxLayout = new QVBoxLayout();
	QVBoxLayout *pSpinBoxLayout = new QVBoxLayout();
	
	int i = 0;
	for (const auto &name : names) {
		QCheckBox *pCheckBox = new QCheckBox(name);
		pCheckBoxLayout->addWidget(pCheckBox);

		spinBoxes[i] = new QDoubleSpinBox();
		spinBoxes[i]->setMinimum(std::numeric_limits<double>::lowest());
		pSpinBoxLayout->addWidget(spinBoxes[i]);
		spinBoxes[i]->setEnabled(false);

		connect(pCheckBox, &QCheckBox::toggled, spinBoxes[i], &QDoubleSpinBox::setEnabled);
		++i;
	}
	QHBoxLayout *pTopLayout = new QHBoxLayout();
	pTopLayout->addLayout(pCheckBoxLayout);
	pTopLayout->addLayout(pSpinBoxLayout);

	pCoefficientSpinBox->setRange(0.1, 1000);
	pCoefficientSpinBox->setValue(100);
	pCoefficientSpinBox->setSingleStep(10);
	QHBoxLayout *pCoefLayout = new QHBoxLayout();
	pCoefLayout->addWidget(new QLabel("Boundaries coefficient"));
	pCoefLayout->addWidget(pCoefficientSpinBox);

	QHBoxLayout *pButtonLayout = new QHBoxLayout();
	pButtonLayout->addWidget(pSave);
	pButtonLayout->addWidget(pCancel);
	connect(pCancel, &QPushButton::released, this, &BoundariesDialog::reject);
	connect(pSave, &QPushButton::released, this, &BoundariesDialog::accept);

	QVBoxLayout *pMainLayout = new QVBoxLayout(this);
	pMainLayout->addLayout(pTopLayout);
	pMainLayout->addLayout(pCoefLayout);
	pMainLayout->addLayout(pButtonLayout);
}

std::shared_ptr<OptimisationBoundaries> BoundariesDialog::getBoundaries() {
	auto boundaries = std::make_shared<OptimisationBoundaries>(OptimisationBoundaries());
	auto setFunctions = {&OptimisationBoundaries::setXMin,
							&OptimisationBoundaries::setXMax,
							&OptimisationBoundaries::setUMin,
							&OptimisationBoundaries::setUMax};
	int i = 0;
	for (auto &setFunction : setFunctions) {
		if (spinBoxes[i]->isEnabled()) {
			((*boundaries).*setFunction)(std::make_unique<double>(spinBoxes[i]->value()));
		}
		else {
			((*boundaries).*setFunction)(nullptr);
		}
		++i;
	}
	boundaries->boundariesCoefficient = pCoefficientSpinBox->value();
	return boundaries;
}

void BoundariesDialog::saveReleased() {
	this->accept();
}
