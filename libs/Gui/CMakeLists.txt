cmake_minimum_required(VERSION 3.13)

project(Gui)

add_library(Gui)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

target_include_directories(Gui
	PUBLIC
		${CMAKE_CURRENT_LIST_DIR}
	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/Private
)

target_sources(Gui
	PRIVATE
		Gui.cpp
		Gui.hpp
		Private/BoundariesDialog.cpp
		Private/BoundariesDialog.hpp
		Private/InitialDataDialog.cpp
		Private/InitialDataDialog.hpp
		Private/OptimisationBrakeDialog.cpp
		Private/OptimisationBrakeDialog.hpp
)

target_link_libraries(Gui
	PUBLIC
		Qt5::Widgets
		Solver
	PRIVATE
		Qt5::Charts
)
