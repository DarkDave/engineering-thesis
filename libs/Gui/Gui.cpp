#include "Gui.hpp"

#include <BoundariesDialog.hpp>
#include <InitialDataDialog.hpp>
#include <OptimisationBrakeDialog.hpp>

#include <QChart>
#include <QChartView>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QScatterSeries>
#include <QTableWidget>
#include <QVBoxLayout>
#include <QValueAxis>
#include <QWidget>

static const double defaultAccuracy = 1e-10;
static const size_t defaultMaxIterations = 10000;
static const uint8_t defaultIterationsOfCoefficientOptimisation = 50;
static const OptimisationBrake defaultBrake(defaultAccuracy, defaultMaxIterations);

using namespace QtCharts;

Gui::Gui(std::shared_ptr<Solver> solver, QWidget *parent)
		:	QMainWindow(parent),
			pSolver(solver),
			pCurrentData(new OptimisationData()),
			pInitialData(new OptimisationData()),
			pBrake(new OptimisationBrake(defaultBrake)),
			pBoundaries(new OptimisationBoundaries()),
			pOptimisationBoundariesLayout(new QHBoxLayout()),
			pAccuracyLabel(nullptr),
			pIterationsLabel(nullptr),
			pCoefficientLabel(nullptr),
			pCoefIterLabel(nullptr),
			dataHistory(),
			pStateChart(new QChart()),
			pInputChart(new QChart()),
			pStateAxis(new QValueAxis()),
			pInputAxis(new QValueAxis()),
			pDataTable(nullptr),
			tableHeader(),
			maxNumberOfDisplayedSteps(3),
			optimisationHorizon(0),
			iterationsOfCoefficientOptimisation(defaultIterationsOfCoefficientOptimisation) {
	pSolver->setOptimisationBoundaries(pBoundaries);
	pSolver->setIterationsOfCoefficientOptimisation(iterationsOfCoefficientOptimisation);

	pInitialData->x = {10};
	pInitialData->u = {13, -10, 5, 7, -4, 0, -9, -3, 5, 2, 8, -3};
	*pCurrentData = *pInitialData;
	optimisationHorizon = pCurrentData->u.size();
	dataHistory.push_back(*pCurrentData);

	initialize();
	updateDisplay();
}

void Gui::initialize() {
	QPushButton *pAdvanceOneStep = new QPushButton("Advance one step");
	QPushButton *pSolve = new QPushButton("Solve");

	QHBoxLayout *pButtonLayout = new QHBoxLayout();
	pButtonLayout->addWidget(pAdvanceOneStep);
	pButtonLayout->addWidget(pSolve);

	connect(pAdvanceOneStep, SIGNAL(released()), this, SLOT(advanceOneStepReleased()));
	connect(pSolve, SIGNAL(released()), this, SLOT(solveReleased()));

	createOptimisationBoundariesLayout();

	QChartView *pStateChartView = new QChartView(pStateChart);
	QChartView *pInputChartView = new QChartView(pInputChart);

	QVBoxLayout *pChartLayout = new QVBoxLayout();
	pChartLayout->addWidget(pStateChartView);
	pChartLayout->addWidget(pInputChartView);

	QLabel *pStateSpaceDesc = new QLabel("<br><h1>State-space representation:</h1><br>");
	QLabel *pStateSpaceEq = new QLabel("<br><h1>x<sub>n+1</sub> = -0.2x<sub>n</sub> + u<sub>n</sub></h1><br>");
	QHBoxLayout *pStateSpaceLayout = new QHBoxLayout();
	pStateSpaceLayout->addWidget(pStateSpaceDesc);
	pStateSpaceLayout->addWidget(pStateSpaceEq);

	QLabel *pQualityIndexDesc = new QLabel("<h1>Quality index:</h1><br>");
	QLabel *pQualityIndexEq = new QLabel("<h1>I = &sum;<sub>n=0</sub><sup>N-1</sup> (x<sub>n</sub><sup>2</sup> + u<sub>n</sub><sup>2</sup>)</h1><br>");
	QHBoxLayout *pQualityIndexLayout = new QHBoxLayout();
	pQualityIndexLayout->addWidget(pQualityIndexDesc);
	pQualityIndexLayout->addWidget(pQualityIndexEq);

	pDataTable = new QTableWidget();
	pDataTable->setShowGrid(true);
	pDataTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

	QVBoxLayout *pTableLayout = new QVBoxLayout();
	pTableLayout->addLayout(pStateSpaceLayout);
	pTableLayout->addLayout(pQualityIndexLayout);
	pTableLayout->addLayout(createOptimisationBrakeLayout());
	pTableLayout->addLayout(pOptimisationBoundariesLayout);
	pTableLayout->addWidget(pDataTable);

	QHBoxLayout *pDataLayout = new QHBoxLayout();
	pDataLayout->addLayout(pChartLayout);
	pDataLayout->addLayout(pTableLayout);

	QPushButton *pSetInitialData = new QPushButton("Set initial data");
	connect(pSetInitialData, SIGNAL(released()), this, SLOT(setInitialData()));
	pButtonLayout->addWidget(pSetInitialData);

	QPushButton *pSetOptimisationBrake = new QPushButton("Set optimisation brake");
	connect(pSetOptimisationBrake, &QPushButton::released, this, &Gui::setOptimisationBrake);
	pButtonLayout->addWidget(pSetOptimisationBrake);

	QPushButton *pSetBoundaries = new QPushButton("Set optimisation boundaries");
	connect(pSetBoundaries, &QPushButton::released, this, &Gui::setOptimisationBoundaries);
	pButtonLayout->addWidget(pSetBoundaries);

	QVBoxLayout *pMainLayout = new QVBoxLayout();
	pMainLayout->addLayout(pDataLayout);
	pMainLayout->addLayout(pButtonLayout);

	QWidget *pMainWidget = new QWidget();
	pMainWidget->setLayout(pMainLayout);
	this->setCentralWidget(pMainWidget);
}

QVBoxLayout *Gui::createOptimisationBrakeLayout() {
	pAccuracyLabel = new QLabel(QString("<h2>Functional gradient accuracy: %1</h2>").arg(pBrake->accuracy));
	pIterationsLabel = new QLabel(QString("<h2>Maximum number of iterations: %1</h2>").arg(pBrake->maxIterations));
	pCoefficientLabel = new QLabel(QString("<h2>Solver's boundaries coefficient: %1</h2>").arg(pBoundaries->boundariesCoefficient));
	pCoefIterLabel = new QLabel(QString("<h2>Number of iterations for calculating gradient improvement coefficient: %1</h2>"
			"<p>0 corresponds to constant value of 0.01.</p><p></p>")
			.arg(iterationsOfCoefficientOptimisation));
	QVBoxLayout *const pLabelLayout = new QVBoxLayout();
	pLabelLayout->addWidget(pAccuracyLabel);
	pLabelLayout->addWidget(pIterationsLabel);
	pLabelLayout->addWidget(pCoefficientLabel);
	pLabelLayout->addWidget(pCoefIterLabel);

	return pLabelLayout;
}

void Gui::createOptimisationBoundariesLayout() {
	const std::vector getters = {&OptimisationBoundaries::getXMin,
						&OptimisationBoundaries::getXMax,
						&OptimisationBoundaries::getUMin,
						&OptimisationBoundaries::getUMax
					};
	const std::vector names = {"Min state", "Max state", "Min input", "Max input"};

	clearLayout(pOptimisationBoundariesLayout);

	QVBoxLayout *pLabelLayout = new QVBoxLayout();
	QVBoxLayout *pValueLayout = new QVBoxLayout();
	for (size_t i = 0; i < getters.size(); ++i) {
		QLineEdit *pLineEdit = new QLineEdit();

		const auto &boundary = ((*pBoundaries).*getters[i])();
		if (boundary) {
			pLineEdit->setText(QString::number(*boundary));
		}
		else {
			pLineEdit->setText("Not active.");
		}
		pLineEdit->setEnabled(false);

		pLabelLayout->addWidget(new QLabel(names[i]));
		pValueLayout->addWidget(pLineEdit);

	}
	pOptimisationBoundariesLayout->addLayout(pLabelLayout);
	pOptimisationBoundariesLayout->addLayout(pValueLayout);
}

void Gui::clearLayout(QLayout *pLayout) {
	QLayoutItem *child;
	while ((child = pLayout->takeAt(0)) != 0) {
    	delete child;
	}
}

void Gui::updateDisplay() {
	uint8_t numberOfSteps = numberOfStepsToShow();

	pStateChart->removeAllSeries();
	pInputChart->removeAllSeries();

	QValueAxis *pStateAxis = new QValueAxis();
	pStateAxis->setRange(0, static_cast<qreal>(optimisationHorizon));
	pStateChart->setAxisX(pStateAxis);

	QValueAxis *pInputAxis = new QValueAxis();
	pInputAxis->setRange(0, static_cast<qreal>(optimisationHorizon));
	pInputChart->setAxisX(pInputAxis);

	for (size_t dataNumber = 0; dataNumber < numberOfSteps; ++dataNumber) {
		auto const &data = dataHistory.at(dataHistory.size() - 1 - dataNumber);

		QScatterSeries *stateSeries = new QScatterSeries();
		stateSeries->setName("State, iteration i-" + QString::number(dataNumber));
		for (size_t i = 0; i<data.x.size(); ++i) {
			*stateSeries << QPointF(static_cast<double>(i), data.x.at(i));
		}
		pStateChart->addSeries(stateSeries);
		stateSeries->attachAxis(pStateAxis);

		QScatterSeries *inputSeries = new QScatterSeries();
		inputSeries->setName("Input, iteration i-" + QString::number(dataNumber));
		for (size_t i = 0; i<data.u.size(); ++i) {
			*inputSeries << QPointF(static_cast<double>(i), data.u.at(i));
		}
		pInputChart->addSeries(inputSeries);
		inputSeries->attachAxis(pInputAxis);
	}

	pStateChart->createDefaultAxes();
	pInputChart->createDefaultAxes();

	this->pStateAxis = pStateAxis;
	this->pInputAxis = pInputAxis;

	updateTable();
}

void Gui::updateTable() {
	uint8_t numberOfSteps = numberOfStepsToShow();
	tableHeader.clear();
	pDataTable->clearContents();
	pDataTable->verticalHeader()->setVisible(false);

	pDataTable->setRowCount(static_cast<int>(optimisationHorizon+1));
	pDataTable->setColumnCount(2*numberOfSteps+1);

	tableHeader << "n";
	for (uint32_t i = 0; i<optimisationHorizon+1; ++i) {
		QTableWidgetItem *number = new QTableWidgetItem(QString("%1").arg(i));
		number->setTextAlignment(Qt::AlignCenter);
		number->setFlags(number->flags() & ~Qt::ItemIsEditable);
		pDataTable->setItem(i, 0, number);
	}

	QTableWidgetItem *prototype = new QTableWidgetItem();
	prototype->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
	prototype->setFlags(prototype->flags() & ~Qt::ItemIsEditable);

	for (uint8_t dataNumber = 0; dataNumber < numberOfSteps; ++dataNumber) {
		tableHeader << "State\n iteration i-" + QString::number(dataNumber);
		tableHeader << "Input\n iteration i-" + QString::number(dataNumber);

		auto const &data = dataHistory.at(dataHistory.size() - 1 - dataNumber);
		for (uint32_t i = 0; i<data.x.size(); ++i) {
			prototype->setText(QString("%1").arg(data.x.at(i)));
			pDataTable->setItem(i, 2*dataNumber+1, prototype->clone());
		}

		for (uint32_t i = 0; i<data.u.size(); ++i) {
			prototype->setText(QString("%1").arg(data.u.at(i)));
			pDataTable->setItem(i, 2*dataNumber+2, prototype->clone());
		}
	}
	pDataTable->setHorizontalHeaderLabels(tableHeader);
}

uint8_t Gui::numberOfStepsToShow() {
	size_t numberOfDataSets = dataHistory.size();
	uint8_t numberOfSteps = maxNumberOfDisplayedSteps;
	if (numberOfDataSets < maxNumberOfDisplayedSteps) {
		numberOfSteps = static_cast<uint8_t>(numberOfDataSets);
	}
	return numberOfSteps;
}

void Gui::advanceOneStepReleased() {
	*pCurrentData = pSolver->advanceOneStep(pCurrentData);
	dataHistory.push_back(*pCurrentData);
	updateDisplay();
}

void Gui::solveReleased() {
	*pCurrentData = pSolver->solve(pCurrentData, *pBrake);
	dataHistory.push_back(*pCurrentData);
	updateDisplay();
}

void Gui::setInitialData() {
	InitialDataDialog dialog;
	auto result = dialog.exec();
	if (result == QDialog::Accepted) {
		*pInitialData = dialog.getInitialData();
		optimisationHorizon = pInitialData->u.size();
		resetToInitialData();
	}
}

void Gui::setOptimisationBrake() {
	OptimisationBrakeDialog dialog;
	const auto result = dialog.exec();
	if (result == QDialog::Accepted) {
		*pBrake = dialog.getOptimisationBrake();
		iterationsOfCoefficientOptimisation = dialog.getIterationsOfCoefficientOptimisation();
		pAccuracyLabel->setText(QString("<h2>Functional gradient accuracy: %1</h2>").arg(pBrake->accuracy));
		pIterationsLabel->setText(QString("<h2>Maximum number of iterations: %1</h2>").arg(pBrake->maxIterations));
		pCoefIterLabel->setText(QString("<h2>Number of iterations for calculating gradient improvement coefficient: %1</h2>"
			"<p>0 corresponds to constant value of 0.01.</p><p></p>")
			.arg(iterationsOfCoefficientOptimisation));

		pSolver->setIterationsOfCoefficientOptimisation(iterationsOfCoefficientOptimisation);
		resetToInitialData();
	}
}

void Gui::setOptimisationBoundaries() {
	BoundariesDialog dialog;
	const auto result = dialog.exec();
	if (result == QDialog::Accepted) {
		pBoundaries = dialog.getBoundaries();
		if(pSolver->setOptimisationBoundaries(pBoundaries) == OptimisationBoundariesAvailable::Unavailable) {
			QMessageBox::warning(this, "", "Solver doesn't implement optimisation boundaries.");
		}
		createOptimisationBoundariesLayout();
		pCoefficientLabel->setText(QString("<h2>Solver's boundaries coefficient: %1</h2>").arg(pBoundaries->boundariesCoefficient));
		resetToInitialData();
	}
}

void Gui::resetToInitialData() {
	*pCurrentData = *pInitialData;
	dataHistory.clear();
	dataHistory.push_back(*pCurrentData);
	updateDisplay();
}
