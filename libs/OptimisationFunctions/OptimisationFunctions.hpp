#ifndef OPTIMISATION_FUNCTIONS_HPP
#define OPTIMISATION_FUNCTIONS_HPP

#include <vector>

class OptimisationFunctions {
public:
	virtual double stateSpaceRepresentation(const std::vector<double> &arguments) const = 0;
	virtual double costFunction(const std::vector<double> &arguments) const = 0;
	virtual double lagrangeMultiplier(const std::vector<double> &arguments) const = 0;
	virtual double functionalGradient(const std::vector<double> &arguments) const = 0;
	
	virtual ~OptimisationFunctions() {};
};

#endif // OPTIMISATION_FUNCTIONS_HPP
