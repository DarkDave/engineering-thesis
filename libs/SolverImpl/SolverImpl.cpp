#include "SolverImpl.hpp"

#include <utility> 		// For std::move.
#include <vector>		// For std::vector.

SolverImpl::SolverImpl(std::shared_ptr<OptimisationFunctions> optimFuncs) : optimFuncs(optimFuncs), bVec() {

}

OptimisationData SolverImpl::solve(std::shared_ptr<OptimisationData> initialData, const OptimisationBrake &brake) {
	OptimisationData outcome(*initialData);

	size_t i = 0;
	for (; i < brake.maxIterations; ++i) {
		outcome = advanceOneStep(std::make_shared<OptimisationData>(outcome));

		double gradientNorm = 0;
		for (auto const & b : bVec) {
			gradientNorm += b*b;
		}
		if (gradientNorm <= brake.accuracy) {
			break;
		}
	}
	return outcome;
}

OptimisationData SolverImpl::advanceOneStep(std::shared_ptr<OptimisationData> currentData) {
	OptimisationData newData(*currentData);
	size_t N = newData.u.size();

	// Calculate StateSpaceRepresentation.
	newData.x.resize(N+1);
	for (size_t i = 0; i < N; ++i) {
		newData.x[i+1] = optimFuncs->stateSpaceRepresentation({newData.x[i], newData.u[i]});
	}
	
	// Calculate Lagrange multipliers.
	std::vector<double> pVec(N+1);
	pVec[N] = 0;
	for (size_t i = N-1; i > 0; --i) {
		pVec[i] = optimFuncs->lagrangeMultiplier({newData.x[i], newData.u[i], pVec[i+1]});
	}
	pVec[0] = optimFuncs->lagrangeMultiplier({newData.x[1], newData.u[1], pVec[1]});

	// Calculate functional gradients.
	bVec.resize(N);
	for (size_t i = 0; i < N; ++i) {
		bVec[i] = optimFuncs->functionalGradient({newData.x[i], newData.u[i], pVec[i+1]});
	}

	// Calculate new Inputs.
	for (size_t i = 0; i < N; ++i) {
		const double gradientImprovementCoefficient = 0.01;
		newData.u[i] = newData.u[i] - gradientImprovementCoefficient * bVec[i];
	}

	// Calculate new StateSpaceRepresentation.
	for (size_t i = 0; i < N; ++i) {
		newData.x[i+1] = optimFuncs->stateSpaceRepresentation({newData.x[i], newData.u[i]});
	}

	return newData;
}

void SolverImpl::setOptimisationFunctions(std::shared_ptr<OptimisationFunctions> optimFuncs) {
	this->optimFuncs = optimFuncs;
}
