#ifndef OPTIMISATION_FUNCTIONS_STUB_HPP
#define OPTIMISATION_FUNCTIONS_STUB_HPP

#include <vector>
#include <OptimisationFunctions.hpp>

class OptimisationFunctionsStub : virtual public OptimisationFunctions {
public:
	double stateSpaceRepresentation(const std::vector<double> &args) const override {
		return 2*args.at(0) - args.at(1);
	}
	double costFunction(const std::vector<double> &args) const override {
		return args.at(0)*args.at(0) + args.at(1)*args.at(1);
	}
	double lagrangeMultiplier(const std::vector<double> &args) const override {
		return 2*args.at(0) + 2*args.at(2);
	}
	double functionalGradient(const std::vector<double> &args) const override {
		return 2*args.at(1) - args.at(2);
	}
};

#endif // OPTIMISATION_FUNCTIONS_STUB_HPP
