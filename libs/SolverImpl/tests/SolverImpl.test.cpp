#include <vector>
#include <memory>
#include <cmath>

#include <gtest/gtest.h>
#include <OptimisationData.hpp>
#include <OptimisationFunctionsStub.hpp>
#include <SolverImpl.hpp>

#include <iostream>
using namespace std;

class TestCase {
public:
	TestCase(const OptimisationData& args, const OptimisationData &result) : args(args), result(result) {};
	OptimisationData args;
	OptimisationData result;
};

bool operator==(const OptimisationData &arg1, const OptimisationData &arg2) {
	const double error = 0.00001;
	if ((arg1.u.size() != arg2.u.size()) || (arg1.x.size() != arg2.x.size())) {
		return false;
	}
	for (size_t i = 0; i < arg1.u.size(); ++i) {
		if (abs(arg1.u[i] - arg2.u[i]) > error) {
			return false;
		}
	}
	for (size_t i = 0; i < arg1.x.size(); ++i) {
		if (abs(arg1.x[i] - arg2.x[i]) > error) {
			return false;
		}
	}
	return true;
}



TEST(AdvanceOneStep, BasicTests) {
	shared_ptr<OptimisationFunctions> optimFuncs = make_shared<OptimisationFunctionsStub>(OptimisationFunctionsStub());
	unique_ptr<Solver> solver = make_unique<SolverImpl>(SolverImpl(optimFuncs));

	vector<TestCase> testCases = {
		TestCase(OptimisationData({1, 3, -1},{5}), OptimisationData(
			{1.76, 3.24, -0.98},
			{5, 8.24, 13.24, 27.46}
		)),
		TestCase(OptimisationData({-1, -4, 2, 3},{-3}), OptimisationData(
			{-2.44, -4.6, 1.68, 2.94},
			{-3, -3.56, -2.52, -6.72, -16.38}
		)),
	};
	
	for (auto const& testCase : testCases) {
		EXPECT_EQ(solver->advanceOneStep((make_shared<OptimisationData>(testCase.args))), testCase.result);
	}
}

TEST(Solve, BasicTests) {
	shared_ptr<OptimisationFunctions> optimFuncs = make_shared<OptimisationFunctionsStub>(OptimisationFunctionsStub());
	unique_ptr<Solver> solver = make_unique<SolverImpl>(SolverImpl(optimFuncs));

	vector<TestCase> testCases = {
		TestCase(OptimisationData({1, 3, -1},{0}), OptimisationData(
			{0, 0, 0},
			{0, 0, 0, 0}
		)),
		TestCase(OptimisationData({1, 3, -1},{5}), OptimisationData(
			{7.5, 2.5, 0},
			{5, 2.5, 2.5, 5}
		)),
		TestCase(OptimisationData({-1, -4, 2, 3},{-3}), OptimisationData(
			{-4.8, -1.8, -0.6, 0},
			{-3, -1.2, -0.6, -0.6, -1.2}
		)),
	};
	
	OptimisationBrake brake(0.0000000001, 10000);

	for (auto const& testCase : testCases) {
		EXPECT_EQ(solver->solve((make_shared<OptimisationData>(testCase.args)), brake), testCase.result);
	}
}
