#include <vector>
#include <memory>

#include <gtest/gtest.h>
#include <OptimisationData.hpp>
#include <OptimisationFunctionsStub.hpp>

// Hack for checking private variables.
#define private public
#include <SolverImpl.hpp>
#undef private

using namespace std;

class TestCase {
public:
	TestCase(const vector<double>& arguments) : arguments(arguments) {};
	
	vector<double> arguments;
};

TEST(Constructor, BasicTests) {
	shared_ptr<OptimisationFunctions> optimFuncs = make_shared<OptimisationFunctionsStub>(OptimisationFunctionsStub());
	unique_ptr<SolverImpl> solver = make_unique<SolverImpl>(SolverImpl(optimFuncs));

	vector<TestCase> testCases = {
		TestCase(vector<double>({1, 2, 4})),
		TestCase(vector<double>({0, 2, 6})),
		TestCase(vector<double>({1.34, 0.1214, -3})),
		TestCase(vector<double>({1.34, 0.1214, 124})),
		TestCase(vector<double>({1.34, 0.1214, -12, 98}))
	};
	
	EXPECT_EQ(solver->optimFuncs, optimFuncs);

	for (auto const& testCase : testCases) {
		EXPECT_DOUBLE_EQ(
			solver->optimFuncs->stateSpaceRepresentation(testCase.arguments),
			optimFuncs->stateSpaceRepresentation(testCase.arguments)
		);
		EXPECT_DOUBLE_EQ(
			solver->optimFuncs->costFunction(testCase.arguments),
			optimFuncs->costFunction(testCase.arguments)
		);
		EXPECT_DOUBLE_EQ(
			solver->optimFuncs->lagrangeMultiplier(testCase.arguments),
			optimFuncs->lagrangeMultiplier(testCase.arguments)
		);
		EXPECT_DOUBLE_EQ(
			solver->optimFuncs->functionalGradient(testCase.arguments),
			optimFuncs->functionalGradient(testCase.arguments)
		);
	}
}
