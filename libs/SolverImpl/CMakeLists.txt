cmake_minimum_required(VERSION 3.13)

project(SolverImpl)

add_library(SolverImpl)

target_include_directories(SolverImpl
	PUBLIC
		${CMAKE_CURRENT_LIST_DIR}
)

target_sources(SolverImpl
	PRIVATE
		SolverImpl.cpp
		SolverImpl.hpp
)

target_link_libraries(SolverImpl
	PUBLIC
		Solver
	PRIVATE
		OptimisationFunctions
)

enable_testing()
add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/tests ${CMAKE_CURRENT_BINARY_DIR}/tests)