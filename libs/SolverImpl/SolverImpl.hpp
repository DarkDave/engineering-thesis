#ifndef SOLVER_IMPL_HPP
#define SOLVER_IMPL_HPP

#include <memory> // For std::shared_ptr
#include <vector>

#include <OptimisationFunctions.hpp>	// For OptimisationFunctions.
#include <OptimisationData.hpp>			// For OptimisationData.
#include <Solver.hpp>					// For base class.

class SolverImpl : virtual public Solver {
public:
	SolverImpl(std::shared_ptr<OptimisationFunctions> optimFuncs);
	OptimisationData solve(std::shared_ptr<OptimisationData> initialData, const OptimisationBrake &brake) override;
	OptimisationData advanceOneStep(std::shared_ptr<OptimisationData> currentData) override;
	void setOptimisationFunctions(std::shared_ptr<OptimisationFunctions> optimFuncs) override;
	
private:
	std::shared_ptr<OptimisationFunctions> optimFuncs;
	std::vector<double> bVec;
};

#endif // SOLVER_IMPL_HPP
