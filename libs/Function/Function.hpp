#ifndef FUNCTION_HPP
#define FUNCTION_HPP

#include <vector>

class Function {
public:
	virtual double calculateValue(const std::vector<double> &arguments) const = 0;
	virtual ~Function() {};
};

#endif // FUNCTION_HPP
