#include <iostream>
#include <memory>

#include <QApplication>
#include <Gui.hpp>
#include <OptimisationFunctionsImpl.hpp>
#include <SolverImpl.hpp>
#include <SolverWithBoundaries.hpp>
#include <OptimisationData.hpp>

using namespace std;

int main(int argc, char *argv[]) {
	shared_ptr optimFuncs = make_shared<OptimisationFunctionsImpl>(OptimisationFunctionsImpl());
	shared_ptr solver = make_shared<SolverWithBoundaries>(SolverWithBoundaries(optimFuncs));

	QApplication app(argc, argv);
	QGuiApplication::setApplicationDisplayName(Gui::tr("Solver"));
	Gui gui(solver);
	gui.showFullScreen();
	app.exec();
}
